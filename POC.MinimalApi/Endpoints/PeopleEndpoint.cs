﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using POC.MinimalApi.Models;

namespace POC.MinimalApi.Endpoints      //This is a test
{
    public class PeopleEndpoint
    {
        private readonly PersonContext _db;

        public static PeopleEndpoint GetPeopleEndpoint(PersonContext db)
        {
            return new PeopleEndpoint(db);
        }

        private PeopleEndpoint(PersonContext db)
        {
            _db = db;
        }

        public async Task<List<Person>> GetPeople()
        {
            return await _db.People.ToListAsync();
        }

        public async Task<IResult> GetPersonById(long id)
        {
            return await _db.People.FindAsync(id)
                            is Person person
                                ? Results.Ok(person)
                                : Results.NotFound();

        }

        public async Task<ActionResult<int>> CreatePerson(Person person)
        {
            Person? existingPerson = await _db.People.FindAsync(person.Id);
            if (existingPerson is null)
            {
                await _db.People.AddAsync(person);
            }
            else
            {
                existingPerson.Gender = person.Gender;
                existingPerson.IsAlive = person.IsAlive;
                existingPerson.Name = person.Name;
            }

            return await _db.SaveChangesAsync();
        }
    }
}