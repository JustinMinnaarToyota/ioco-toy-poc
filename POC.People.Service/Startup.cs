﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using POC.People.Service.Models;
using POC.People.Service.Services;

namespace POC.People.Service
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<PersonContext>(opt => opt.UseInMemoryDatabase("People"));
            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddScoped<IPeopleService, PeopleService>();
        }
    }
}