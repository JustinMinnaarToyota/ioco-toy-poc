﻿using Microsoft.EntityFrameworkCore;

using POC.People.Service.Models;

namespace POC.People.Service.Services
{
    public class PeopleService : IPeopleService
    {
        private readonly PersonContext _db;

        public PeopleService(PersonContext db)
        {
            _db = db;
        }

        public async Task<List<Person>> GetPeopleAsync() => await GetPeopleAsync(CancellationToken.None);
        public async Task<List<Person>> GetPeopleAsync(CancellationToken cancellationToken)
        {
            return await _db.People.ToListAsync(cancellationToken).ConfigureAwait(false);
        }

        public async Task<Person?> GetPersonAsync(long id) => await GetPersonAsync(id, CancellationToken.None);
        public async Task<Person?> GetPersonAsync(long id, CancellationToken cancellationToken)
        {
            return await _db.People.FindAsync(new object[] { id }, cancellationToken).ConfigureAwait(false);
        }

        public async Task<int> CreatePersonAsync(Person person) => await CreatePersonAsync(person, CancellationToken.None);
        public async Task<int> CreatePersonAsync(Person person, CancellationToken cancellationToken)
        {
            Person? existingPerson = await _db.People.FindAsync(new object[] { person.Id }, cancellationToken)
                                                        .ConfigureAwait(false);
            if (existingPerson is null)
            {
                await _db.People.AddAsync(person, cancellationToken)
                    .ConfigureAwait(false);
            }
            else
            {
                existingPerson.Gender = person.Gender;
                existingPerson.IsAlive = person.IsAlive;
                existingPerson.Name = person.Name;
            }

            return await _db.SaveChangesAsync(cancellationToken)
                .ConfigureAwait(false);
        }
    }
}