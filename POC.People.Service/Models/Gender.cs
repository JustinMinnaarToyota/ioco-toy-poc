﻿using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace POC.People.Service.Models
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum Gender
    {
        [EnumMember(Value = "Unset")]
        Unset = 0,
        [EnumMember(Value = "Male")]
        Male,
        [EnumMember(Value = "Female")]
        Female,
    }
}
