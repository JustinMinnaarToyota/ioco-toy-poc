﻿using System.ComponentModel.DataAnnotations;

namespace POC.People.Service.Models
{
    public class Person
    {
        [Key]
        public long Id { get; set; }
        public string? Name { get; set; }

        public Gender Gender { get; set; }

        public bool IsAlive { get; set; }
    }
}
