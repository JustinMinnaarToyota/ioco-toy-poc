﻿namespace POC.WebApp.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public Enums.Gender Gender { get; set; } = Enums.Gender.Unset;
        public bool IsAlive { get; set; }
    }
}
