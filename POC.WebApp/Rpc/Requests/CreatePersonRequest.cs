﻿using POC.WebApp.Models;

namespace POC.WebApp.Rpc.Requests
{
    public class CreatePersonRequest : Request
    {
        public Person Person { get; set; } = default!;
    }
}
