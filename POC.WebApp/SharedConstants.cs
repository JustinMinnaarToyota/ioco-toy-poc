﻿namespace POC.WebApp
{
    public class SharedConstants
    {
        private SharedConstants() { }

        public const string ApiBaseUrl = "https://localhost:7168";
        public const string MinimalApiBaseUrl = "https://localhost:7098";
    }
}
