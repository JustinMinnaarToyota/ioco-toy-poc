﻿namespace POC.WebApp.Services
{
    public class ConfigService
    {
        public string ApiBaseUrl { get; set; }
        
        public ConfigService(string apiBaseUrl)
        {
            this.ApiBaseUrl = apiBaseUrl;
        }
    }
}
