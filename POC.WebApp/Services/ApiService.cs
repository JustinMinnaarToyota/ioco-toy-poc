﻿using System.Net.Mime;
using System.Text;

using Newtonsoft.Json;

using POC.WebApp.Models;
using POC.WebApp.Rpc.Requests;

namespace POC.WebApp.Services
{
    public class ApiService
    {
        private readonly ConfigService config;
        private readonly HttpClient httpClient;

        public ApiService(ConfigService config, IHttpClientFactory httpClientFactory)
        {
            this.config = config;
            this.httpClient = httpClientFactory.CreateClient();
        }

        public async Task<Person> CreatePerson(Person? person, CancellationToken cancellationToken)
        {
            if (person is null)
            {
                throw new ArgumentNullException(nameof(person));
            }
            if (String.IsNullOrWhiteSpace(person.Name))
            {
                throw new ArgumentException("Value is invalid.", nameof(person.Name));
            }
            if (person.Gender == Models.Enums.Gender.Unset)
            {
                throw new ArgumentException("Value is invalid.", nameof(person.Gender));
            }

            Uri uri = new Uri(new Uri(this.config.ApiBaseUrl), "api/people");
            using HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, uri)
            {
                Content = new StringContent(
                    JsonConvert.SerializeObject(person),
                    Encoding.UTF8,
                    MediaTypeNames.Application.Json)
            };
            using HttpResponseMessage responseMessage = await this.httpClient
                .SendAsync(requestMessage, cancellationToken)
                .ConfigureAwait(false);
            responseMessage.EnsureSuccessStatusCode();

            string responseContent = await responseMessage.Content
                .ReadAsStringAsync(cancellationToken)
                .ConfigureAwait(false);

            Person? createdPerson = JsonConvert.DeserializeObject<Person>(responseContent);
            if (createdPerson is null)
            {
                throw new Exception("Unable to deserialize response content.");
            }

            return createdPerson;
        }

        public async Task<Person[]> GetPeople(CancellationToken cancellationToken)
        {
            Uri uri = new Uri(new Uri(this.config.ApiBaseUrl), "api/people");
            using HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, uri);
            using HttpResponseMessage responseMessage = await this.httpClient
                .SendAsync(requestMessage, cancellationToken)
                .ConfigureAwait(false);
            responseMessage.EnsureSuccessStatusCode();

            string responseContent = await responseMessage.Content
                .ReadAsStringAsync(cancellationToken)
                .ConfigureAwait(false);

            Person[]? people = JsonConvert.DeserializeObject<Person[]>(responseContent);
            if (people is null)
            {
                throw new Exception("Unable to deserialize response content.");
            }

            return people;
        }

        public async Task<Person> GetPerson(int personId, CancellationToken cancellationToken)
        {
            if (personId <= 0)
            {
                throw new ArgumentException($"{personId} is an invalid value.", nameof(personId));
            }

            Uri uri = new Uri(new Uri(this.config.ApiBaseUrl), $"api/people/{personId}");
            using HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Get, uri);
            using HttpResponseMessage responseMessage = await this.httpClient
                .SendAsync(requestMessage, cancellationToken)
                .ConfigureAwait(false);
            responseMessage.EnsureSuccessStatusCode();

            string responseContent = await responseMessage.Content
                .ReadAsStringAsync(cancellationToken)
                .ConfigureAwait(false);

            if (String.IsNullOrWhiteSpace(responseContent))
            {
                throw new Exception($"Received no response content for person: {personId}");
            }

            Person? person = JsonConvert.DeserializeObject<Person>(responseContent);
            if (person is null)
            {
                throw new Exception("Unable to deserialize response content.");
            }

            return person;
        }

        public async Task<Person> UpdatePerson(Person? person, CancellationToken cancellationToken)
        {
            if (person is null)
            {
                throw new ArgumentNullException(nameof(person));
            }
            if (person.Id <= 0)
            {
                throw new ArgumentException("Person id is invalid.", nameof(person.Id));
            }
            if (person.Gender == Models.Enums.Gender.Unset)
            {
                throw new ArgumentException("Gender is invalid.", nameof(person.Gender));
            }

            Uri uri = new Uri(new Uri(this.config.ApiBaseUrl), "api/people");
            using HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Put, uri)
            {
                Content = new StringContent(
                    JsonConvert.SerializeObject(person),
                    Encoding.UTF8,
                    MediaTypeNames.Application.Json)
            };
            using HttpResponseMessage responseMessage = await this.httpClient
                .SendAsync(requestMessage, cancellationToken)
                .ConfigureAwait(false);
            responseMessage.EnsureSuccessStatusCode();

            string responseContent = await responseMessage.Content
                .ReadAsStringAsync(cancellationToken)
                .ConfigureAwait(false);

            Person? createdPerson = JsonConvert.DeserializeObject<Person>(responseContent);
            if (createdPerson is null)
            {
                throw new Exception("Unable to deserialize response content.");
            }

            return createdPerson;
        }
    }
}
